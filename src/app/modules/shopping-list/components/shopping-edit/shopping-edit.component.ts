import { Component, OnDestroy, OnInit } from '@angular/core';
import { FormControl, FormGroup, Validators } from '@angular/forms';
import { Store } from '@ngrx/store';
import { Ingredient } from 'src/app/modules/recipes/models/ingredients.model';
import { AppState } from 'src/app/store/app.reducer';
import * as ShoppingListActions from '../../store/shopping-list.actions';

@Component({
  selector: 'app-shopping-edit',
  templateUrl: './shopping-edit.component.html',
  styleUrls: ['./shopping-edit.component.scss'],
})
export class ShoppingEditComponent implements OnInit, OnDestroy {
  shoppingForm: FormGroup;
  editMode = false;

  constructor(private store: Store<AppState>) {}

  ngOnInit() {
    this.shoppingForm = new FormGroup({
      name: new FormControl('', [Validators.required, Validators.minLength(2)]),
      amount: new FormControl(null, [Validators.required, Validators.min(1)]),
    });
    this.store.select('shoppingList').subscribe((state) => {
      if (state.editedIngredientIndex > -1) {
        this.loadSelectedIngredient(state.editedIngredient);
      }
    });
  }

  ngOnDestroy() {
    this.store.dispatch(new ShoppingListActions.StopEdition());
  }

  loadSelectedIngredient(ingredient: Ingredient) {
    this.editMode = true;
    this.shoppingForm.setValue({
      name: ingredient.name,
      amount: ingredient.amount,
    });
  }

  onSubmit() {
    if (this.shoppingForm.valid) {
      if (this.editMode) {
        this.store.dispatch(
          new ShoppingListActions.UpdateIngredient({
            ...this.shoppingForm.value,
          })
        );
      } else {
        this.store.dispatch(
          new ShoppingListActions.AddIngredient({ ...this.shoppingForm.value })
        );
      }
      this.resetForm();
    }
  }

  deleteIngredient() {
    if (this.editMode) {
      this.store.dispatch(new ShoppingListActions.DeleteIngredient());
      this.resetForm();
    }
  }

  resetForm() {
    this.store.dispatch(new ShoppingListActions.StopEdition());
    this.shoppingForm.reset();
    this.editMode = false;
  }
}
