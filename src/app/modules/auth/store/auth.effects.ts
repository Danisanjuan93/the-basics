import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Router } from '@angular/router';
import { Actions, createEffect, ofType } from '@ngrx/effects';
import { of } from 'rxjs';
import { catchError, map, switchMap, tap } from 'rxjs/operators';
import { AuthResponse } from 'src/app/shared/interfaces/auth.interface';
import { environment } from 'src/environments/environment';
import { AuthService } from '../services/auth.service';
import {
  LOGIN,
  Login,
  LoginFail,
  LoginStart,
  LOGIN_START,
} from './auth.actions';

@Injectable()
export class AuthEffects {
  authLogin = createEffect(() =>
    this.actions$.pipe(
      ofType(LOGIN_START),
      switchMap((authData: LoginStart) => {
        return this.http
          .post<AuthResponse>(environment.signInUrl, {
            ...authData.payload,
            returnSecureToken: true,
          })
          .pipe(
            map((resData) => {
              this.authService.storeData(resData);
              return new Login({ email: resData.email, id: resData.localId });
            }),
            tap(() => this.router.navigate(['/'])),
            catchError((errorRes) => {
              let errorMessage = 'An error occured!';
              if (!errorRes.error || !errorRes.error.error) {
                return of(new LoginFail(errorMessage));
              }
              switch (errorRes.error.error.message) {
                case 'EMAIL_EXISTS':
                  errorMessage = 'Email already exists';
                  break;
                case 'OPERATION_NOT_ALLOWED':
                  errorMessage = 'Operation not allowed';
                  break;
                case 'TOO_MANY_ATTEMPTS_TRY_LATER':
                  errorMessage = 'Too many attempts. Try later';
                  break;
                case 'EMAIL_NOT_FOUND':
                case 'INVALID_PASSWORD':
                  errorMessage = 'Invalid credentials';
                  break;
                case 'USER_DISABLED':
                  errorMessage = 'User is disabled';
                  break;
                default:
                  break;
              }
              return of(new LoginFail(errorMessage));
            })
          );
      })
    )
  );

  // authRegister = createEffect(() =>
  //   this.actions$.pipe(
  //     ofType(LOGIN_START),
  //     switchMap((authData: LoginStart) => {
  //       return this.http
  //         .post<AuthResponse>(environment.signUpUrl, { ...authData })
  //         .pipe(
  //           map((resData) => {
  //             return new Login({ email: resData.email, id: resData.localId });
  //           }),
  //           catchError(() => {
  //             let errorMessage = 'An error occured!';
  //             return of(new LoginFail(errorMessage));
  //           })
  //         );
  //     })
  //   )
  // );

  constructor(
    private actions$: Actions,
    private http: HttpClient,
    private router: Router,
    private authService: AuthService
  ) {}
}
