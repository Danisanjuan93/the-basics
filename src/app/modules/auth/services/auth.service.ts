import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { CookieService } from 'ngx-cookie-service';
import { Observable } from 'rxjs';
import {
  AuthRequest,
  AuthResponse,
} from 'src/app/shared/interfaces/auth.interface';

@Injectable({ providedIn: 'root' })
export class AuthService {
  private signInUrl =
    'https://identitytoolkit.googleapis.com/v1/accounts:signInWithPassword?key=AIzaSyCfrz8nnVCA_B_uDGxv23oy_kM4SmjYtGI';
  private signUpUrl =
    'https://identitytoolkit.googleapis.com/v1/accounts:signUp?key=AIzaSyCfrz8nnVCA_B_uDGxv23oy_kM4SmjYtGI';

  expiresInTimer: any;

  constructor(private http: HttpClient, private cookieService: CookieService) {}

  signIn(data: AuthRequest): Observable<AuthResponse> {
    return this.http.post<AuthResponse>(this.signInUrl, {
      ...data,
      returnSecureToken: true,
    });
  }

  signUp(data: AuthRequest): Observable<AuthResponse> {
    data['returnSecureToken'] = true;
    return this.http.post<AuthResponse>(this.signUpUrl, data);
  }

  validateCookie() {
    return !!this.cookieService.get('userCookie');
  }

  getUserCookie() {
    return JSON.parse(this.cookieService.get('userCookie'));
  }

  storeData(data: AuthResponse) {
    const expiresIn = new Date(new Date().getTime() + +data.expiresIn * 1000);
    this.cookieService.set(
      'userCookie',
      JSON.stringify({
        token: data.idToken,
        refreshToken: data.refreshToken,
        userEmail: data.email,
        userId: data.localId,
      }),
      expiresIn
    );
    this.autoLogout(+data.expiresIn * 1000);
  }

  private autoLogout(expiresIn: number) {
    this.expiresInTimer = setTimeout(() => {
      this.logout();
    }, expiresIn);
  }

  logout() {
    this.cookieService.deleteAll();
    this.expiresInTimer && clearTimeout(this.expiresInTimer);
  }
}
