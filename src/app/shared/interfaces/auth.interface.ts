export interface AuthRequest {
  email: string;
  password: string;
  returnSecureToken: boolean;
}

export interface AuthResponse {
  displayName: string;
  kind: string;
  idToken: string;
  email: string;
  refreshToken: string;
  expiresIn: string;
  localId: string;
  registered: boolean;
}
