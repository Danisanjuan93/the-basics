import { User } from 'src/app/shared/models/user.model';
import {
  AuthActions,
  LOGIN,
  LOGIN_FAIL,
  LOGIN_START,
  LOGOUT,
  REGISTER,
} from './auth.actions';

export interface StoreAuthState {
  user: User;
  isLogged: boolean;
  authError: string;
}

const initialState: StoreAuthState = {
  user: null,
  isLogged: false,
  authError: null,
};

export function authReducer(state = initialState, action: AuthActions) {
  switch (action.type) {
    case LOGIN:
      const { email, id } = action.payload;
      const user = new User(email, id);
      return {
        ...state,
        user: user,
        isLogged: true,
        authError: null,
      };
    case LOGOUT:
      return {
        ...state,
        user: null,
        isLogged: false,
        authError: null,
      };
    case LOGIN_START:
      return {
        ...state,
        authError: null,
      };
    case LOGIN_FAIL:
      return {
        ...state,
        user: null,
        isLogged: false,
        authError: action.payload,
      };
    case REGISTER:
      return {
        ...state,
        user: null,
        isLogged: false,
        authError: null,
      };
    default:
      return state;
  }
}
