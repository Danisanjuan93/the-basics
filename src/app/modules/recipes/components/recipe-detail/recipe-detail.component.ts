import { Component, Input, OnInit } from '@angular/core';
import { ActivatedRoute, Params, Router } from '@angular/router';
import { Store } from '@ngrx/store';
import { Recipe } from 'src/app/modules/recipes/models/recipe.model';
import { RecipeService } from 'src/app/modules/recipes/services/recipe.service';
import { AddIngredients } from 'src/app/modules/shopping-list/store/shopping-list.actions';
import { AppState } from 'src/app/store/app.reducer';

@Component({
  selector: 'app-recipe-detail',
  templateUrl: './recipe-detail.component.html',
  styleUrls: ['./recipe-detail.component.scss'],
})
export class RecipeDetailComponent implements OnInit {
  @Input() selectedRecipe!: Recipe;
  id: number = 0;

  constructor(
    private recipeService: RecipeService,
    private route: ActivatedRoute,
    private router: Router,
    private store: Store<AppState>
  ) {}

  ngOnInit() {
    this.id = +this.route.snapshot.params['id'];
    this.route.params.subscribe(
      (params: Params) =>
        (this.selectedRecipe = this.recipeService.getRecipe(+params['id']))
    );
  }

  addToShoppingList() {
    this.store.dispatch(new AddIngredients(this.selectedRecipe.ingredients));
  }

  editRecipe() {
    this.recipeService.recipeSubject.next(this.selectedRecipe);
    this.router.navigate(['edit'], { relativeTo: this.route });
  }

  deleteRecipe() {
    this.recipeService.deleteRecipe(this.id);
    this.router.navigate(['recipes']);
  }
}
