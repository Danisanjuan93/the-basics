import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { map, Observable, tap } from 'rxjs';
import { Recipe } from 'src/app/modules/recipes/models/recipe.model';
import { RecipeService } from '../../modules/recipes/services/recipe.service';

@Injectable({ providedIn: 'root' })
export class DataStorageService {
  url =
    'https://ng-course-recipe-book-d8484-default-rtdb.europe-west1.firebasedatabase.app/recipes.json';

  constructor(private http: HttpClient, private recipeService: RecipeService) {}

  getRecipes(): Observable<Recipe[]> {
    return this.http.get<Recipe[]>(this.url).pipe(
      map((recipes) => {
        return recipes.map((recipe) => {
          return { ...recipe, ingredients: recipe.ingredients || [] };
        });
      }),
      tap((recipes) => this.recipeService.setRecipes(recipes))
    );
  }

  storeRecipes(): Observable<{ name: string }> {
    const recipes = this.recipeService.getRecipes();
    return this.http.put<{ name: string }>(this.url, recipes);
  }
}
