import { Component, OnInit } from '@angular/core';
import { Store } from '@ngrx/store';
import { AuthService } from './modules/auth/services/auth.service';
import { Login, Logout } from './modules/auth/store/auth.actions';
import { AppState } from './store/app.reducer';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.scss'],
})
export class AppComponent implements OnInit {
  constructor(
    private authService: AuthService,
    private store: Store<AppState>
  ) {}

  ngOnInit() {
    if (this.authService.validateCookie()) {
      const userCookie = this.authService.getUserCookie();
      const email = userCookie.userEmail;
      const id = userCookie.userId;
      this.store.dispatch(new Login({ email, id }));
    } else {
      this.store.dispatch(new Logout());
      this.authService.logout();
    }
  }
}
