import { CommonModule } from '@angular/common';
import { NgModule } from '@angular/core';
import { ReactiveFormsModule } from '@angular/forms';
import { SharedModule } from 'src/app/shared/components/shared.module';
import { AuthRoutingModule } from './auth-routing.module';
import { AuthComponent } from './components/auth.component';

@NgModule({
  declarations: [AuthComponent],
  imports: [ReactiveFormsModule, SharedModule, CommonModule, AuthRoutingModule],
  exports: [AuthComponent],
})
export class AuthModule {}
