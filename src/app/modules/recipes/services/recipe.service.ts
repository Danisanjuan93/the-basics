import { Injectable } from '@angular/core';
import { Store } from '@ngrx/store';
import { Subject } from 'rxjs';
import { AppState } from 'src/app/store/app.reducer';
import { Recipe } from '../models/recipe.model';

@Injectable({ providedIn: 'root' })
export class RecipeService {
  recipeSubject = new Subject<Recipe>();
  recipesSubject = new Subject<Recipe[]>();
  private recipes: Recipe[] = [];

  constructor(private store: Store<AppState>) {}

  setRecipes(recipes: Recipe[]) {
    this.recipes = recipes;
    this.recipesSubject.next(recipes);
  }

  getRecipes(): Recipe[] {
    return this.recipes.slice();
  }

  getRecipe(id: number): Recipe {
    return this.recipes.filter((recipe: Recipe) => recipe.id === id)[0];
  }

  addRecipe(recipe: Recipe): void {
    this.recipes.push({ ...recipe, id: this.recipes.length + 1 });
    this.recipesSubject.next(this.recipes.slice());
  }

  editRecipe(recipeUpdated: Recipe): void {
    const index = this.recipes.findIndex(
      (recipe: Recipe) => recipe.id === recipeUpdated.id
    );
    this.recipes[index] = { ...recipeUpdated };
    this.recipesSubject.next(this.recipes);
  }

  deleteRecipe(id: number) {
    const index = this.recipes.findIndex((recipe: Recipe) => id === recipe.id);
    this.recipes.splice(index, 1);
    this.recipesSubject.next(this.recipes);
  }
}
