import { Action } from '@ngrx/store';
import { Ingredient } from '../../recipes/models/ingredients.model';

export const ADD_INGREDIENT = '[Shopping List] Add Ingredient';
export const UPDATE_INGREDIENT = '[Shopping List] Update Ingredient';
export const DELETE_INGREDIENT = '[Shopping List] Delete Ingredient';

export const ADD_INGREDIENTS = '[Shopping List] Add Ingredients';

export const START_EDITION = '[Shopping List] Start Edition';
export const STOP_EDITION = '[Shopping List] Stop Edition';

export type Actions =
  | AddIngredient
  | AddIngredients
  | UpdateIngredient
  | DeleteIngredient
  | StartEdition
  | StopEdition;

export class AddIngredient implements Action {
  readonly type = ADD_INGREDIENT;

  constructor(public payload: Ingredient) {}
}

export class AddIngredients implements Action {
  readonly type = ADD_INGREDIENTS;

  constructor(public payload: Ingredient[]) {}
}

export class UpdateIngredient implements Action {
  readonly type = UPDATE_INGREDIENT;

  constructor(public payload: Ingredient) {}
}

export class DeleteIngredient implements Action {
  readonly type = DELETE_INGREDIENT;
}

export class StartEdition implements Action {
  readonly type = START_EDITION;

  constructor(public payload: number) {}
}

export class StopEdition implements Action {
  readonly type = STOP_EDITION;
}
