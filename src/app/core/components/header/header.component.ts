import { Component } from '@angular/core';
import { Router } from '@angular/router';
import { Subscription } from 'rxjs';
import { AuthService } from '../../../modules/auth/services/auth.service';
import { DataStorageService } from '../../services/data-storage.service';
import { RecipeService } from '../../../modules/recipes/services/recipe.service';
import { Store } from '@ngrx/store';
import { AppState } from 'src/app/store/app.reducer';
import { Logout } from 'src/app/modules/auth/store/auth.actions';

@Component({
  selector: 'app-header',
  templateUrl: './header.component.html',
  styleUrls: ['./header.component.scss'],
})
export class HeaderComponent {
  isLogged = false;
  subscription: Subscription = new Subscription();

  constructor(
    private dataStorageService: DataStorageService,
    private recipeService: RecipeService,
    private authService: AuthService,
    private router: Router,
    private store: Store<AppState>
  ) {}

  ngOnInit() {
    this.store.select('auth').subscribe((data) => {
      this.isLogged = data.isLogged;
    });
  }

  ngOnDestroy() {
    this.subscription && this.subscription.unsubscribe();
  }

  onFetchData() {
    this.dataStorageService
      .getRecipes()
      .subscribe((response) => this.recipeService.setRecipes(response));
  }

  onSaveData() {
    this.dataStorageService.storeRecipes().subscribe();
  }

  logout() {
    this.store.dispatch(new Logout());
    this.authService.logout();
    this.router.navigate(['auth']);
  }
}
