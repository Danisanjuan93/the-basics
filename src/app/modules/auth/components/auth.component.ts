import { Component, OnInit } from '@angular/core';
import { FormControl, FormGroup, Validators } from '@angular/forms';
import { Store } from '@ngrx/store';
import { AppState } from 'src/app/store/app.reducer';
import { LoginStart } from '../store/auth.actions';

@Component({
  selector: 'app-auth',
  templateUrl: './auth.component.html',
  styleUrls: ['./auth.component.scss'],
})
export class AuthComponent implements OnInit {
  authForm: FormGroup;
  isLoginMode = true;
  isLoading = false;
  error = '';
  constructor(private store: Store<AppState>) {}

  ngOnInit() {
    this.authForm = new FormGroup({
      email: new FormControl('', [
        Validators.required,
        Validators.minLength(5),
        Validators.email,
      ]),
      password: new FormControl('', Validators.required),
    });
    this.store.select('auth').subscribe((data) => {
      this.error = data.authError;
      this.isLoading = false;
    });
  }

  onSubmit() {
    if (this.authForm.valid) {
      this.isLoading = true;
      if (this.isLoginMode) {
        this.store.dispatch(new LoginStart({ ...this.authForm.value }));
      } else {
      }
      //   this.authService[this.isLoginMode ? 'signIn' : 'signUp']({
      //     ...this.authForm.value,
      //   })
      //     .pipe(
      //       finalize(() => (this.isLoading = false)),
      //       catchError((error) => {
      //         console.log(error)
      //         this.error = error.error.error.message;
      //         return throwError(() => {
      //           return this.error;
      //         });
      //       })
      //     )
      //     .subscribe((response: AuthResponse) => {
      //       this.authService.storeData(response);
      //       this.store.dispatch(
      //         new LoginAction({
      //           email: this.authForm.value['email'],
      //           id: response.localId,
      //         })
      //       );
      //       this.error = '';
      //       this.router.navigate(['recipes']);
      //       this.authForm.reset();
      //     });
      // }
    }
  }

  switchModes() {
    this.isLoginMode = !this.isLoginMode;
  }
}
