import { ActionReducerMap } from '@ngrx/store';
import {
  authReducer,
  StoreAuthState,
} from '../modules/auth/store/auth.reducer';
import {
  shoppingListReducer,
  StoreShoppingListState,
} from '../modules/shopping-list/store/shopping-list.reducer';

export interface AppState {
  shoppingList: StoreShoppingListState;
  auth: StoreAuthState;
}

export const appReducer: ActionReducerMap<AppState> = {
  shoppingList: shoppingListReducer,
  auth: authReducer,
};
