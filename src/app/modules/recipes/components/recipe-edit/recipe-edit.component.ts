import { Component, OnInit } from '@angular/core';
import { FormArray, FormControl, FormGroup, Validators } from '@angular/forms';
import { ActivatedRoute, Params, Router } from '@angular/router';
import { RecipeService } from 'src/app/modules/recipes/services/recipe.service';
import { Recipe } from 'src/app/modules/recipes/models/recipe.model';

@Component({
  selector: 'app-recipe-edit',
  templateUrl: './recipe-edit.component.html',
  styleUrls: ['./recipe-edit.component.scss'],
})
export class RecipeEditComponent implements OnInit {
  id: number = 0;
  editMode = false;

  recipe!: Recipe;
  recipeForm: FormGroup = new FormGroup({});

  constructor(
    private route: ActivatedRoute,
    private router: Router,
    private recipeService: RecipeService
  ) {}

  ngOnInit() {
    this.route.params.subscribe((params: Params) => {
      this.id = +params['id'];
      this.editMode = params['id'] != null;
    });

    this.initForm(this.id ? this.recipeService.getRecipe(this.id) : null);
  }

  initForm(recipe: Recipe | null) {
    this.recipeForm = new FormGroup({
      id: new FormControl(recipe?.id || 0, Validators.required),
      name: new FormControl(recipe?.name, [
        Validators.required,
        Validators.minLength(2),
      ]),
      imagePath: new FormControl(recipe?.imagePath, Validators.required),
      description: new FormControl(recipe?.description, [
        Validators.required,
        Validators.minLength(2),
      ]),
      ingredients: new FormArray([]),
    });
    if (recipe?.ingredients) {
      for (const ingredient of recipe.ingredients) {
        (this.recipeForm.controls['ingredients'] as FormArray).push(
          new FormGroup({
            name: new FormControl(ingredient.name, [
              Validators.required,
              Validators.minLength(2),
            ]),
            amount: new FormControl(ingredient.amount, [
              Validators.required,
              Validators.min(1),
            ]),
          })
        );
      }
    }
  }

  get ingredientItems() {
    return (this.recipeForm.controls['ingredients'] as FormArray).controls;
  }

  onSubmit() {
    if (this.recipeForm.valid) {
      if (this.editMode) {
        this.recipeService.editRecipe({ ...this.recipeForm.value });
        this.editMode = false;
        this.router.navigate([`recipes/${this.id}`]);
      } else {
        this.recipeService.addRecipe({ ...this.recipeForm.value });
        this.router.navigate(['recipes']);
      }
    }
  }

  resetForm() {
    this.recipeForm.reset();
    (this.recipeForm.controls['ingredients'] as FormArray).clear();
  }

  cancel() {
    this.router.navigate(['recipes']);
  }

  addIngredient() {
    (this.recipeForm.controls['ingredients'] as FormArray).push(
      new FormGroup({
        name: new FormControl('', [
          Validators.required,
          Validators.minLength(2),
        ]),
        amount: new FormControl(null, [Validators.required, Validators.min(1)]),
      })
    );
  }

  deleteRecipe() {
    if (this.editMode) {
      this.recipeService.deleteRecipe(this.id);
      this.router.navigate(['recipes']);
    }
  }

  deleteIngredient(index: number) {
    (this.recipeForm.controls['ingredients'] as FormArray).removeAt(index);
  }
}
