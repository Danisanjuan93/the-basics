import { Component, OnDestroy, OnInit } from '@angular/core';
import { RecipeService } from 'src/app/modules/recipes/services/recipe.service';
import { Recipe } from 'src/app/modules/recipes/models/recipe.model';

@Component({
  selector: 'app-recipes',
  templateUrl: './recipes.component.html',
  styleUrls: ['./recipes.component.scss'],
})
export class RecipesComponent implements OnInit, OnDestroy {
  selectedRecipe!: Recipe;

  constructor(private recipeService: RecipeService) {}

  ngOnInit() {}

  ngOnDestroy() {}
}
